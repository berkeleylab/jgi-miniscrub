# A wrapper script for the shell_setup.sh script
import argparse, subprocess, sys


def parseargs():    # handle user arguments
	parser = argparse.ArgumentParser(
		description='Setup script for MiniScrub dependencies if docker not used.')
	parser.add_argument('--noroot', action='store_true',
		help = 'Use if you do not have root access or want to install nonroot.')
	parser.add_argument('--nogpu', action='store_true',
		help = 'Use if GPUs not available.')
	parser.add_argument('--full', action='store_true',
		help = 'Install extra tools used in experiments.')
	args = parser.parse_args()
	return args


def main():
	args = parseargs()
	rootvar = 'noroot' if args.noroot else 'root'
	gpuvar = 'nogpu' if args.nogpu else 'gpu'
	fullvar = 'full' if args.full else 'basic'
	subprocess.call(['./shell_setup.sh', rootvar, gpuvar, fullvar])


if __name__ == '__main__':
	main()

#
