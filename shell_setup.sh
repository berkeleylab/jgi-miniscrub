#!/bin/sh
# Setup script for dependencies. It is recommended that you run this through
# 	the wrapper script nodocker-setup.py

git clone https://github.com/JGI-Bioinformatics/minimap2.git && cd minimap2 && git checkout OutputMinimizerPositionsOption && make && cd ..
if [ "$1" = "noroot" ]:
then
	pip3 install --user setuptools numpy scipy pandas h5py matplotlib pillow scikit-learn
	if [ "$2" = "nogpu" ]:
	then
		pip3 install --user tensorflow
	else
		pip3 install --user tensorflow-gpu
	fi
	pip3 install --user keras
else
	pip3 install setuptools numpy scipy pandas h5py matplotlib pillow scikit-learn
	if [ "$2" = "nogpu" ]:
	then
		pip3 install tensorflow
	else
		pip3 install tensorflow-gpu
	fi
	pip3 install keras
fi


if [ "$3" = "full" ]:
then
	# Setup MECAT, Canu, and GraphMap for tests run in paper
	git clone https://github.com/xiaochuanle/MECAT.git && cd MECAT && make && cd ..
	wget https://github.com/marbl/canu/releases/download/v1.7/canu-1.7.Linux-amd64.tar.xz && xz -dc canu-1.7.*.tar.xz |tar -xf - && rm canu-1.7.*
	git clone https://github.com/isovic/graphmap.git && cd graphmap && make modules && make && cd ..
	# Setup other nanopore preprocessing tools
	git clone https://github.com/rrwick/Porechop.git
	if [ "$1" = "noroot" ]:
	then
		pip3 install --user nanopack
	else
		pip3 install nanopack
		cd Porechop && python3 setup.py install
	fi
fi
#
