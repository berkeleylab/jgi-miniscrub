import argparse, gc, glob, gzip, math, multiprocessing, os, random, subprocess, sys, time, traceback
import numpy as np
import pandas as pd
from scipy import ndimage
from scipy.stats import pearsonr, spearmanr
#from sklearn.metrics import roc_auc_score

import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
stderr = sys.stdout
sys.stderr = open('/dev/null', 'w')
import keras
from keras.models import load_model
from sklearn.metrics import roc_auc_score
sys.stderr = stderr
start = time.time()


def echo(msg, prepend='', postpend=''):
	global start
	seconds = time.time() - start
	m, s = divmod(seconds, 60)
	h, m = divmod(m, 60)
	hms = "%02d:%02d:%02d" % (h, m, s)
	print(prepend + '[' + hms + '] ' + msg + postpend)


def parseargs():
	parser = argparse.ArgumentParser(description='Use saved keras model to scrub reads.')
	parser.add_argument('reads', help='Path to reads file. Required.')
	parser.add_argument('--compression', default='none', choices=['none', 'gzip'], help='Compression of reads file ("none" or "gzip").')
	parser.add_argument('--cutoff', default=0.8, type=float, help='Scrub read segments below this percent identity.')
	parser.add_argument('--debug', default=0, type=int, help='Number of images to use in debug mode. If <= 0, non-debug mode (default).')
	parser.add_argument('--input', default='./', help='Directory with png pileup images if --pileup=load. Default: current directory.')
	parser.add_argument('--labels', default='NONE', help='Path to image labels file. If provided, will NOT trim. Labels must correspond with segment_size.')
	parser.add_argument('--limit_length', default=0, type=int, help='Optionally do not include reads above a certain length.')
	parser.add_argument('--limit_fastq', default=0, type=int, help='Optionally limit the number of reads to process from --reads.')
	parser.add_argument('--limit_paf', default=0, type=int, help='Optionally limit the number of reads from paf file (if --paf is used).')
	parser.add_argument('--load', default='NONE', help='Path to keras model file to load. Default is included pretrained model.')
	parser.add_argument('--mask', action='store_true', help='Mask bad sections with Ns instead of removing.')
	parser.add_argument('--min_length', default=500, type=int, help='Minimum length of reads to keep. Default: 500.')
	parser.add_argument('--mode', default='minimizers', choices=['minimizers', 'whole'], help='Whether pileups are minimizers-only or whole reads.')
	parser.add_argument('--noscrub', action='store_true', help='Do not scrub (output statsitics instead).')
	parser.add_argument('--output', default='scrubbed-reads.fastq', help='File to write scrubbed reads to.')
	parser.add_argument('--paf', default='NONE', help='Path to paf file; minimap2 will be run if not specified.')#required if --mode=minimizers and --reads is specified, and --pileups=load.')
	parser.add_argument('--pileup', default='generate', choices=['generate', 'load'], help='Whether pileups should be generated or read from files created by pileup.py.')
	parser.add_argument('--processes', default=16, type=int, help='Number of processes to use for multiprocessing. Default: 16.')
	parser.add_argument('--segment_size', default=48, type=int, help='Neural net segment size to predict. Keep as default unless network retrained.')
	parser.add_argument('--verbose', action='store_true', help='Verbose output option.')
	parser.add_argument('--window_size', default=72, type=int, help='Neural net window size to predict. Keep as default unless network retrained.')
	args = parser.parse_args()
	return args


def read_paf(args):
	if args.compression == 'none':
		paf = open(args.paf, 'r')
	else:
		paf = gzip.open(args.paf, 'r')

	linecount = 0
	minimizers = {}
	for line in paf:
		if args.compression == 'gzip':
			line = line.decode('utf8')
		splits = line.strip().split('\t')
		if splits[0] == splits[5]:  # read mapped against itself
			if splits[0] in minimizers or (args.limit_length > 0 and int(splits[1]) > args.limit_length):
				continue
			if splits[-6][6] == 'I':
				minimizers[splits[0]] = [int(i) for i in splits[-6][8:].split(',')]
			else:
				minimizers[splits[0]] = [int(i) for i in splits[-6][7:].split(',')]

			linecount += 1
			if args.verbose and linecount % 10000 == 0:
				echo('Done reading ' + str(linecount) + ' lines from --paf file')
			if args.limit_paf > 0 and linecount % args.limit_paf == 0:
				paf.close()
				return minimizers

	paf.close()
	return minimizers


def get_read_info(args):  # using fastq file, map read names to sequence and quality scores
	if args.verbose:
		echo('Reading --reads file...')
	if args.compression == 'gzip':
		reads_file = gzip.open(args.reads, 'r')
	else:
		reads_file = open(args.reads, 'r')
	reads_df, num, current = {}, -1, ''
	read_count = 0
	for line in reads_file:
		if args.compression == 'gzip':
			line = line.decode('utf8').strip()
		num = (num + 1) % 4
		if num == 0:
			current = line[1:].split(' ')[0]
			read_count += 1
			if read_count % 10000 == 0 and args.verbose:
				echo('Done scanning ' + str(read_count) + ' reads from --reads file.')
		#elif num == 1:
		#	reads_df[current] = [line.upper()]
		elif num == 3:
			#scores = [int((ord(ch)-33)*2.75) for ch in line]
			#scores = [(ord(ch)-33) for ch in line]
			scores = [ord(ch) for ch in line]
			reads_df[current] = [scores, np.mean(scores)]
			if args.limit_fastq > 0 and read_count > args.limit_fastq:
				break
	reads_file.close()
	if args.verbose:
		echo('Finished reading --reads file.')
	return reads_df


def setup_generation(args):
	labels_dict = {}
	'''if testing == True:
		labels_file = open(args.labels, 'r')
		for line in labels_file:
			splits = line.strip().split(' ')
			if len(splits) < 2:
				continue
			labels_dict[splits[0]] = [float(i) for i in splits[1].split(',')]
		labels_file.close()'''

	reads_df = get_read_info(args)
	reads_list = list(reads_df)
	return labels_dict, reads_list, reads_df


def generate_windows(args, imarray, imname):
	zero_segments, pos = [1, 1], 0
	data, locations = [], []
	# break read into windows, excluding junk 0s at the ends
	sidelen = (args.window_size - args.segment_size) / 2  # extra space on each side of segment in window
	prev_end, num_segments = 0, int(math.ceil(float(len(imarray[0])) / float(args.segment_size)))
	blanks = [[0,0,0]] * int((48 - args.window_size) / 2)
	for i in range(zero_segments[0], num_segments-zero_segments[1]):
		startpos, endpos = int((i*args.segment_size)-sidelen), int(((i+1)*args.segment_size)+sidelen)
		if startpos < 0:
			continue
		if endpos > len(imarray[0]):
			break
		window = imarray[:,startpos:endpos]

		if len(blanks) > 0:
			window = list(window)
			for j in range(len(window)):
				window[j] = list(window[j])
				window[j] = np.concatenate((blanks, window[j], blanks), axis=0)
			window = np.array(window)
		if len(window) < 48:
			blankrows = [[[0,0,0]] * len(window[0])] * (48 - len(window))
			window = np.concatenate((window, blankrows), axis=0)

		#if args.testing == True:
		#	label = imlabels[i]
		#	labels.append(label)
		data.append(window)
		locations.append(str(imname)+' | '+str(startpos+sidelen)+' | '+str(endpos-sidelen))#-1))
	return data, locations


def stretch_factor_minimizers(startpos, line, all_mins, refdict, selection, color='rgb'):
	ref_mins, match_mins = selection[12], selection[13]
	match_start = selection[13][refdict[all_mins[startpos]]]
	for endpos in range(startpos+1, len(line)):
		if color == 'rgb' and line[endpos][0] == 255.0:
			match_end = selection[13][refdict[all_mins[endpos]]]
			stretch = (match_end - match_start) / (endpos - startpos)
			return endpos, min(255, round(abs(stretch)*10))
		elif color == 'bw' and line[endpos] >= 128.0:
			match_end = selection[13][refdict[all_mins[endpos]]]
			stretch = (match_end - match_start) / (endpos - startpos)
			return endpos, min(127.0, round(abs(stretch)*5))
	return len(line), 0


def initialize_pileup(minimizers, readname, readqual, readlen, matches, args):
	k, maxdepth, pileup, avg_stretch = 15, 24, [], 1.0  # 128.0
	seq = [[255.0, np.mean(readqual[i:i+k])*2.0, avg_stretch] if i+k <= len(readqual) else [255.0, np.mean(readqual[i:len(readqual)])*2.0, avg_stretch] for i in minimizers]
	for i in range(len(minimizers)-1):
		seq[i][2] = min(255, (minimizers[i+1] - minimizers[i])*10)
	seq[-1][2] = min(255, (readlen - minimizers[-1])*10)
	pileup.append(seq)
	for i in range(maxdepth):
		pileup.append([[0.0,0.0,0.0]])  # fill in placeholder lines
	return pileup


def fill_pileup(pileup, minimizers, readname, readqual, readlen, matches, args):
	k, maxdepth, avg_stretch = 15, 24, 1.0
	depth_order, depth_index, num = list(range(1, maxdepth+1)), 0, 0
	minidict = {minimizers[i]: i for i in range(len(minimizers))}
	for s in matches:
		selection = matches[s]
		# sel.[12] == ref. seq. minimizers, sel.[13] == match. seq. minimizers
		sel12dict = {selection[12][i]: i for i in range(len(selection[12]))}
		meanqual = np.mean(selection[14])*2.0
		match_start, match_end = minidict[selection[12][0]], minidict[selection[12][-1]]
		seq = [[255.0, meanqual, avg_stretch] if minimizers[i] in selection[12] else [70.0, meanqual, avg_stretch] for i in range(match_start, match_end+1)]
		seq = ([[0.0, 0.0, 0.0]] * match_start) + seq + ([[0.0, 0.0, 0.0]] * (len(minimizers) - match_end - 1))

		for pix in range(len(seq)):
			if seq[pix][0] == 255.0 and seq[pix][2] != 0.0:
				matchind = selection[13][sel12dict[minimizers[pix]]]
				seq[pix][1] = np.mean(selection[14][matchind:matchind+k])*2.0 if matchind+k < len(selection[14]) else np.mean(selection[14][matchind:len(selection[14])])*2.0

		pix = 0
		while pix + 1 < len(seq):
			if seq[pix][2] != 0.0:
				endpos, stretch = stretch_factor_minimizers(pix, seq, minimizers, sel12dict, selection)
				seq[pix:endpos] = [[i[0], i[1], stretch] for i in seq[pix:endpos]]
				if endpos <= pix:
					pix += 1
				else:
					pix = endpos
			else:
				pix += 1

		pileup[depth_order[depth_index]] = seq
		depth_index += 1
		if depth_index >= maxdepth:
			break

	for line in range(len(pileup)):
		pileup[line].extend([[0.0,0.0,0.0]] * (len(minimizers) - len(pileup[line])))
	return pileup


def make_pileup_rgb_minimizers(pid, pq, fail_list, readname, readqual, readlen, matches, args):
	try:
		minimizers = matches[0][12]
		del matches[0]
		pileup = initialize_pileup(minimizers, readname, readqual, readlen, matches, args)
		pileup = fill_pileup(pileup, minimizers, readname, readqual, readlen, matches, args)
		pileup = np.array(pileup)
		windows, locs = generate_windows(args, pileup, readname)
		pq.put([windows, locs], block=True)  # if queue full, block until space free
		#time.sleep(0.1)  # let pq.put finish
		return 0
	except:
		#print('Error in process ' + str(pid))
		#print('For read named: ' + str(readname))
		#err = sys.exc_info()
		#tb = traceback.format_exception(err[0], err[1], err[2])
		#print(''.join(tb) + '\n')
		#sys.stdout.flush()
		fail_list[0] += 1
		return 1


def process_paf_line(args, line, read_data, cur_read, reads_df, reads_list):
	if args.compression == 'gzip':
		line = line.decode('utf8')
	splits = line.strip().split('\t')
	#splits = splits[:12] + splits[13:15]
	splits = splits[:14]
	for i in (1,2,3,6,7,8,9,10,11):
		splits[i] = float(splits[i])
	if splits[12][6] == 'I':
		splits[12] = splits[12][1:]; splits[13] = splits[13][1:]
	splits[12] = [int(i) for i in splits[12][7:].split(',')]  # ref. seq. minimizers
	splits[13] = [int(i) for i in splits[13][7:].split(',')]  # match. seq. minimizers
	if splits[0] not in reads_df or splits[5] not in reads_df:
		return -1, -1, -1, read_data
	splits.append(reads_df[splits[5]][0])
	if args.limit_fastq > 0 and (splits[0] not in reads_list or splits[5] not in reads_list):
		return -1, -1, -1, read_data
	if splits[12][0] > splits[12][-1]:
		splits[12].reverse()  # assure ascending minimizer order
		splits[13].reverse()

	readqual, readlen = [], 0
	if read_data != {} and cur_read != splits[0]:
		readqual, readlen = reads_df[cur_read][0], len(reads_df[cur_read][0])
		selections = list(range(1,len(read_data)))
		random.shuffle(selections)
		selections = selections[:args.maxdepth] + [0]
		read_data = {i:read_data[i] for i in selections}
	return splits, readqual, readlen, read_data


def generate_images(args):
	reslist = []
	args.maxdepth = 24
	batch_size, windows, preds, locs = 64, [], [], []
	labels_dict, reads_list, reads_df = setup_generation(args)
	echo('Loading model...')
	model = load_model(args.load)
	echo('Model loaded.')

	context = multiprocessing.get_context("spawn")
	pool = context.Pool(processes=args.processes)
	manager = multiprocessing.Manager()
	pq = manager.Queue()#1000)  # pileup results queue, arg is maximum queue size
	fail_list = manager.list(range(1))
	read_count, line_count, read_data, cur_read = 0, 0, {}, ''
	if args.compression == 'gzip':
		f = gzip.open(args.paf, 'r')
	else:
		f = open(args.paf, 'r')
	for line in f:
		results = process_paf_line(args, line, read_data, cur_read, reads_df, reads_list)
		if results[2] == -1:
			continue
		splits, readqual, readlen, read_data = results[0], results[1], results[2], results[3]
		if readlen == -1:
			continue
		if read_data != {} and cur_read != splits[0]:
			if args.processes <= 1:
				make_pileup_rgb_minimizers(read_count, pq, fail_list, cur_read, readqual, readlen, read_data, args)
			else:
				res = pool.apply_async(make_pileup_rgb_minimizers, (read_count, pq, fail_list, cur_read, readqual, readlen, read_data, args,))
				reslist.append(res)
				#print(res.get(2))
				#time.sleep(1)
				#if not pq.empty():
				#	print(pq.get()[1])
				#sys.exit()
			read_count += 1
			if read_count % 1000 == 0 and args.verbose:
				echo('Finished pileups for ' + str(read_count) + ' lines')
			if args.debug > 0 and read_count >= args.debug:
				break
			read_data, line_count = {}, 0
		read_data[line_count] = splits
		cur_read = splits[0]
		line_count += 1

		if read_count % 100 == 0:  # routine check of queue and running GPU predictions
			reslen = len(reslist)  # length of reslist at a point in time
			for res in range(reslen):  # don't process new incoming results yet
				reslist[res].get(timeout=60)
			reslist = reslist[reslen:]

			pqsize = pq.qsize()
			#print(pqsize)
			for i in range(pqsize):
				data = pq.get()
				windows.extend(data[0])
				locs.extend(data[1])
			while len(windows) >= batch_size:
				batch = np.array(windows[:batch_size])
				windows = windows[batch_size:]
				bp = model.predict(batch, batch_size=64)
				bp = np.array([i[0] for i in bp])
				preds.extend(bp)

	if read_data != {} and (read_count < args.debug or args.debug == 0):
		if args.processes <= 1:
			make_pileup_rgb_minimizers(read_count, pq, fail_list, cur_read, readqual, readlen, read_data, args)
		else:
			res = pool.apply_async(make_pileup_rgb_minimizers, (read_count, pq, fail_list, cur_read, readqual, readlen, read_data, args,))
			reslist.append(res)
	f.close()
	pool.close()
	pool.join()
	time.sleep(1)
	if fail_list[0] > 0:
		print('Warning: ' + str(fail_list[0]) + ' pileups could not be built.')
		print('Remaining results will not be affected.')
	for res in range(len(reslist)):
		reslist[res].get(timeout=60)
	del reslist
	while not pq.empty():
		data = pq.get()
		windows.extend(data[0])
		locs.extend(data[1])
	while len(windows) > 0:
		batch = np.array(windows[:batch_size])
		windows = windows[batch_size:]
		bp = model.predict(batch, batch_size=64, verbose=0)
		bp = np.array([i[0] for i in bp])
		preds.extend(bp)
	return model, preds, locs


def process_images(args, labels_dict, testing=False, fnames=None):
	data, svmdata, labels, locations, endpoints = [], [], [], [], []  # endpoints is position where full reads end
	if fnames == None:
		fnames = glob.glob(args.input+'*.png')
	for fname in fnames:
		imname = fname.split('/')[-1][:-4]
		zero_segments, pos = [1, 1], 0
		if testing == True and imname not in labels_dict:
			continue
		elif testing == True:
			imlabels = labels_dict[imname]
			for i in imlabels:  # here we determine the 0 identity segments on the end of reads, which are junk
				if i == 0:
					zero_segments[pos] += 1
				else:
					pos=1

		imarray = ndimage.imread(fname, mode='RGB')
		imdata, imlocs = generate_windows(args, imarray, imname) # break read into windows, excluding junk 0s at the ends
		data.extend(imdata)
		locations.extend(imlocs)
		endpoints.append(len(data))
		if args.debug > 0 and len(endpoints) >= args.debug:
			break

	if len(data) == 0:
		print('Error: no data found.')
		sys.exit()

	data = np.array(data)
	svmdata = np.array(svmdata)
	labels = np.array(labels)
	return data, svmdata, labels, endpoints, locations


def get_data(args, testing=False, fnames=None):
	labels_dict = {}
	if testing == True:
		labels_file = open(args.labels, 'r')
		for line in labels_file:
			splits = line.strip().split(' ')
			if len(splits) < 2:
				continue
			labels_dict[splits[0]] = [float(i) for i in splits[1].split(',')]
		labels_file.close()

	data, svmdata, labels, endpoints, locations = process_images(args, labels_dict, testing, fnames)
	return data, svmdata, labels, locations


def print_err_and_correlation(actual, predicted, baseline=False):
	errors = [abs(actual[i]-predicted[i]) for i in range(len(actual))]
	print('Average error: ' + str(np.mean(errors)))
	mse = np.mean([i**2 for i in errors])
	print('Mean squared error: ' + str(mse) + '\n')
	percented, within1, within5, within10 = 100.0 / float(len(actual)), 0.0, 0.0, 0.0
	for i in range(len(actual)):
		if errors[i] < 0.01:
			within1 += percented
		if errors[i] < 0.05:
			within5 += percented
		if errors[i] < 0.1:
			within10 += percented
	print(str(within1) + ' percent of predictions within 1.0 of actual')
	print(str(within5) + ' percent of predictions within 5.0 of actual')
	print(str(within10) + ' percent of predictions within 10.0 of actual')
	print(str(100.0 - within10) + ' percent of predictions outside 10.0 from actual')
	print('\nPearson correlation: ' + str(pearsonr(actual, predicted)[0]))
	print('Spearman rank correlation: ' + str(spearmanr(actual, predicted)[0]))
	print('\nClassification metrics for various cutoff thresholds:\n')


def print_classification_accuracy(actual, predicted, baseline=False):
	cutoffs, df = [0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9], {}
	for val in cutoffs:
		tp, fp, tn, fn = 0.0, 0.0, 0.0, 0.0
		for i in range(len(actual)):
			if actual[i] >= val and predicted[i] >= val:
				tp += 1.0
			elif actual[i] < val and predicted[i] >= val:
				fp += 1.0
			elif actual[i] < val and predicted[i] < val:
				tn += 1.0
			elif actual[i] >= val and predicted[i] < val:
				fn += 1.0
		accuracy, precision, recall, f1, specificity, aucroc = ['nan' for i in range(6)]
		if tp + fp + tn + fn > 0:
			accuracy = (tp + tn) / (tp + fp + tn + fn)
		if tp + fp > 0:
			precision = tp / (tp + fp)
		if tp + fn > 0:
			recall = tp / (tp + fn)
		if precision != 'nan' and recall != 'nan' and precision + recall > 0:
			f1 = 2 * precision * recall / (precision + recall)
		if fp + tn > 0:
			specificity = tn / (tn + fp)
		binary_actual = [1 if actual[i] > val else 0 for i in range(len(actual))]
		if not (sum(binary_actual) == 0 or sum(binary_actual) == len(binary_actual)):
			aucroc = roc_auc_score(binary_actual, predicted)
		df[val] = [accuracy, precision, recall, specificity, aucroc]
	df = pd.DataFrame.from_dict(df, orient='index')
	df = df.sort_index()
	df.index.name = 'Cutoff'
	df.columns = ['Accuracy', 'Precision', 'Recall/Sensitivity', 'Specificity', 'AUC-ROC']
	print(df)


def eval_preds(actual, predicted, baseline=False):
	print_err_and_correlation(actual, predicted, baseline=False)
	print_classification_accuracy(actual, predicted, baseline=False)


def load_and_test(args):
	fnames = glob.glob(args.input+'*.png')
	if args.debug > 0:
		fnames = fnames[:args.debug]
	counter, predictions, locations = 0, [], []
	if args.verbose:
		echo('Loading model...')
	model = load_model(args.load)
	if args.verbose:
		echo('Model loaded successfully. Predicting...')
	while counter < len(fnames):
		batch = fnames[counter:counter+100]
		if args.labels != 'NONE':
			data, svmdata, labels, locs = get_data(args, testing=True, fnames=batch)
		else:
			data, svmdata, labels, locs = get_data(args, testing=False, fnames=batch)
		locations.extend(locs)

		preds = model.predict(data, batch_size=64)
		preds = np.array([i[0] for i in preds])
		predictions.extend(preds)
		counter += 100

	if args.labels != 'NONE':
		if args.verbose:
			echo('Evaluating predictions on provided labels...')
		eval_preds(labels, predictions)
	return model, predictions, locations


def locate_predictions(predictions, locations, minimizers):
	pred_locs, deletions = {}, 0
	for loc in range(len(locations)):
		loc -= deletions
		name, start, end = locations[loc].split(' | ')
		start, end = int(float(start)), int(float(end))
		if minimizers != {} and name not in minimizers:
			del predictions[loc], locations[loc]
			deletions += 1
			continue
		elif minimizers != {}:
			try:
				start, end = minimizers[name][start], minimizers[name][end]
			except:
				print(minimizers[name])
				print(name, start, end, len(minimizers[name]))
				sys.exit()
		if name not in pred_locs:
			pred_locs[name] = [[predictions[loc], start, end]]
		else:
			pred_locs[name].append([predictions[loc], start, end])
	return pred_locs, predictions, locations


def output_statistics(args, predictions, pred_locs):
	print('\nTotal number of predictions made: ' + str(len(predictions)))
	print('Average prediction (of percentage of correct bases per read segment): ' + str(100.0*np.mean(predictions)))
	print('Median prediction: ' + str(100.0*np.median(predictions)))
	print('\nEstimated percentage of read segments to be scrubbed at different cutoff points:')
	print('(Takes into account --min_length='+str(args.min_length)+')')
	cutoffs = [0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9]
	for cutoff in cutoffs:
		cur_segments, cur_length, total_segments = 0, 0, 0
		for readname in pred_locs:
			for segment in pred_locs[readname]:
				pred, start, end = segment
				if pred > cutoff:
					cur_segments += 1
					cur_length += (end - start)
				else:
					if cur_length >= args.min_length:
						total_segments += cur_segments
					cur_segments, cur_length = 0, 0
			if cur_length >= args.min_length:
				total_segments += cur_segments
			cur_segments, cur_length = 0, 0
		print(str(cutoff) + ': ' + str(100.0 - (100.0 * (float(total_segments) / float(len(predictions))))))


def scrub_read(args, read, pred_locs, cutoff):
	prevend = 0
	scrubbed_reads, locs = [''], [[-1, -1]]
	if len(scrubbed_reads[0]) < pred_locs[0][1]:
		scrubbed_reads[0] = read[:pred_locs[0][1]]
		locs[0] = [0, pred_locs[0][1]]
	for i in range(len(pred_locs)):
		pred, start, end = pred_locs[i]
		#if prevend != start-1:
		#	print prevend, pred_locs[i]
		if pred > cutoff:
			scrubbed_reads[-1] += read[start:end]
			if locs[-1][0] == -1:
				locs[-1][0] = start
			locs[-1][1] = end
		elif args.mask:
			scrubbed_reads[-1] += ('N'*(end-start))
			if locs[-1][0] == -1:
				locs[-1][0] = start
			locs[-1][1] = end
		else:
			scrubbed_reads.append('')
			locs.append([-1, -1])
		prevend = end
	if len(read) > pred_locs[-1][2]:
		scrubbed_reads[-1] += read[pred_locs[-1][2]:]
		if locs[-1][0] == -1:
			locs[-1][0] = pred_locs[-1][2]
		locs[-1][1] = len(read)

	if len(scrubbed_reads) > 1:
		for i in range(len(scrubbed_reads)):
			if len(scrubbed_reads[i]) <= 50:
				continue
			scrubbed_reads[i] = scrubbed_reads[i][:-50]
			locs[i][1] -= 50

	selections = [i for i in range(len(scrubbed_reads)) if len(scrubbed_reads[i]) >= args.min_length]
	scrubbed_reads = [scrubbed_reads[i] for i in selections]
	locs = [locs[i] for i in selections]
	#scrubbed_reads = [i for i in scrubbed_reads if len(i) >= args.min_length]
	return scrubbed_reads, locs


def output_reads(args, pred_locs):
	if args.compression == 'none':
		f = open(args.reads, 'r')
	else:
		f = gzip.open(args.reads, 'r')
	outfile = open(args.output, 'w')

	cur_read, read_line, readlen, line3, num = '', '', 0, '', -1
	scrubbed_reads, scrubbed_quals, locs = [], [], []
	for line in f:
		if args.compression == 'gzip':
			line = line.decode('utf8')
		num = (num + 1) % 4
		if num == 0:
			cur_read = line[1:].split(' ')[0].strip()
			read_line = line[len(cur_read)+1:].strip()
		elif num == 1:
			if cur_read in pred_locs:
				readlen = len(line.strip())
				scrubbed_reads, locs = scrub_read(args, line.strip(), pred_locs[cur_read], args.cutoff)
			else:
				scrubbed_reads = []
		elif num == 2:
			line3 = line.strip()
		elif len(scrubbed_reads) > 0:
			scrubbed_quals, locs = scrub_read(args, line.strip(), pred_locs[cur_read], args.cutoff)
			for i in range(len(scrubbed_reads)):
				if len(scrubbed_reads) > 1 or len(scrubbed_reads[0]) < readlen:
					line1 = '@' + cur_read + '_bases-' + str(locs[i][0]) + '-to-' + str(locs[i][1]) + ' ' + read_line
				else:
					line1 = '@' + cur_read + ' ' + read_line
				outfile.write('\n'.join([line1, scrubbed_reads[i], line3, scrubbed_quals[i]]) + '\n')
	f.close(); outfile.close()


if __name__ == '__main__':
	args = parseargs()
	if args.min_length < 1:
		print('Error: Minimum read length to keep must be at least 1.')
		sys.exit()
	if not args.input.endswith('/'):
		args.input += '/'
	if args.reads == 'NONE':
		print('Reads to trim not provided. Will print some statsitics.')

	__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__))) + '/'
	if args.load == 'NONE':
		args.load = __location__ + 'pretrained-model.hd5'
	minimap = False
	if args.labels == 'NONE' and args.mode =='minimizers' and args.paf == 'NONE':
		minimap = True

	minimizers = {}
	if args.labels == 'NONE' and args.mode == 'minimizers':
		if minimap:
			if args.verbose:
				echo('Running minimap (minimizers mode selected, no --paf specified).')
			cmd = __location__ + 'minimap2/minimap2 --output-min --all-chain -k15 -w5 -m100 -g10000 -r2000 --max-chain-skip 25 ' + args.reads + ' ' + args.reads
			with(open('minimap-temp.paf', 'w')) as paffile:
				with(open(os.devnull, 'w')) as DEVNULL:
					subprocess.Popen(cmd.split(' '), stdout=paffile, stderr=DEVNULL).wait()
			args.paf = 'minimap-temp.paf'
			if args.compression == 'gzip':
				pipecmd = 'gzip -1 minimap-temp.paf'
				subprocess.Popen(pipecmd.split(' ')).wait()
				args.paf += '.gz'
		if args.verbose:
			echo('Reading paf file...')
		minimizers = read_paf(args)
		if args.verbose:
			echo('Finished reading paf file.')

	if args.pileup == 'generate':
		model, predictions, locations = generate_images(args)
	else:
		model, predictions, locations = load_and_test(args)
	if args.labels == 'NONE':
		if args.verbose:
			echo('Predictions made. Locating segments to cut...')
		predictions = list(predictions)
		pred_locs, predictions, locations = locate_predictions(predictions, locations, minimizers)
		if args.noscrub == 'NONE':
			output_statistics(args, predictions, pred_locs)
		else:
			if args.verbose:
				echo('Scrubbing reads...')
			output_reads(args, pred_locs)
	if minimap:
		subprocess.Popen(['rm', args.paf])
	if args.verbose:
		echo('Done.')
#
